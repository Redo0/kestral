--love 1

function love.conf(t)
	t.console = true
	local w = t.window
	w.width = 1024
	w.height = 768
	w.title = "Kestral Simulator"
end
