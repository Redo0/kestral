--love 1

local classes = require "classes"

local lg = love.graphics
local le = love.event

local scale = 50
local function p(t) for i,v in ipairs(t) do t[i]=v*scale end return t end

local wire = classes.Wire(4,
	{
		p{1,1},
		p{1,2},
		p{2,2},
		p{2,3},
		p{3,3},
		p{3,2},
	},
	{
		{1,2},
		{1,2,3},
		{2,3,4},
		{3,4,5},
		{4,5,6},
		{5,6},
	}
)

local chip = classes.Chip("Test", p{5, 5, 1, 2})

local chips = {
	classes.Chip("W    ", p{1.5, 1.5, 1, 1}),
	classes.Chip("X    ", p{1.5, 3.5, 1, 1}),
	classes.Chip("Y    ", p{1.5, 7.5, 1, 1}),
	classes.Chip("Z"    , p{4.5, 5.5, 1, 1}),
	classes.Chip("Shift", p{4  , 3  , 2, 2}),
}

function love.load()
	lg.setFont(lg.newFont(16))
end

ap = 1
gt = 0

function love.update(dt)
	gt = gt+dt
	if gt>0.1 then
		gt = 0
		ap = ap%(#wire.paths) + 1
		wire:setActivePath(ap)
	end
end

function love.draw()
	for chipidx, chip in ipairs(chips) do
		chip:draw()
	end
end

function love.keypressed(k)
	if k=="escape" then le.quit() end
end
