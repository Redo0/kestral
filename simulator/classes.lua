--love 1

local lg = love.graphics

local class = require "class"

local classes = {}

local function drawline(x1, y1, x2, y2, radius)
	lg.rectangle("fill", x1-radius, y1-radius, x2-x1+radius*2, y2-y1+radius*2)
end

local function drawpath(points, path, radius)
	for i = 1, #path-1 do
		local x1, y1 = points[path[i  ]][1], points[path[i  ]][2]
		local x2, y2 = points[path[i+1]][1], points[path[i+1]][2]
		
		drawline(x1, y1, x2, y2, radius)
	end
end

classes.Wire = class{
	points = {},
	paths = {},
	pathon = nil,
	radius = 1,
	value = 0,
	
	new = function(wire, thickness, points, paths)
		wire.points = points
		wire.paths = paths
		wire.radius = thickness/2
		wire.value = 0
	end,
	
	setValue = function(wire, value)
		wire.value = value
	end,
	
	setActivePath = function(wire, path)
		wire.pathon = path
	end,
	
	draw = function(wire)
		lg.setColor(0.4, 0.4, 0.4, 1)
		for pathidx, path in ipairs(wire.paths) do
			if pathidx~=wire.pathon then
				drawpath(wire.points, path, wire.radius)
			end
		end
		lg.setColor(1, 1, 1, 1)
		if wire.pathon then
			drawpath(wire.points, wire.paths[wire.pathon], wire.radius)
		end
	end,
}

classes.Chip = class{
	name = "empty",
	box = {},
	value = 0,
	
	new = function(chip, name, box)
		chip.name = name
		chip.box = box
		chip.value = 0
	end,
	
	setValue = function(chip, value)
		chip.value = value
	end,
	
	draw = function(chip)
		lg.setColor(0.7, 0.7, 0.7, 1)
		lg.setLineWidth(4)
		lg.rectangle("line", chip.box[1], chip.box[2], chip.box[3], chip.box[4])
		lg.print(chip.name, chip.box[1]+6, chip.box[2]+chip.box[4]/2-10)
	end,
}

return classes
