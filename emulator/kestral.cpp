
#include <malloc.h>
#include <stdio.h>
#include <conio.h>

const bool outputdebug = false;

const int word_length = 16;
const int stack_size = 1024;
const int mem_size = 1024*1024;

const int word_max = 1<<word_length;
const int word_half = word_max>>1;

int makesigned(int n){
	return n>=word_half ? n-word_max : n;
	return n;
}

int inputn(){
	int i;
	scanf("%d", &i);
	return i;
}
int inputc(){
	return getch();
}
void outputn(int n){
	printf("%d ", makesigned(n));
}
void outputc(int c){
	putchar((char)(c%256));
}

int memory[mem_size];

int stack[stack_size];
int stackptr;

int ipstack[stack_size];
int ipstackptr;

char* program;

int pc;
int search;

char c;

int actualmod(int a, int b){
	return ((a%b)+b)%b;
}
int correct(int n){
	return actualmod(n, word_max);
}

void push(int n){
	stack[++stackptr] = correct(n);
}
int pop(){
	return stack[stackptr--];
}
int pop2(){
	int t = stack[stackptr-1];
	stack[stackptr-1] = stack[stackptr];
	stackptr--;
	return t;
}
int peek(){
	return stack[stackptr];
}
int get(int n){
	return stack[stackptr-n+1];
}
void set(int n, int m){
	stack[stackptr-n+1] = m;
}
void swap(int n){
	int t = stack[stackptr-n+1];
	stack[stackptr-n+1] = stack[stackptr];
	stack[stackptr] = t;
}

void puship(int n){
	ipstack[++ipstackptr] = n;
}
int popip(){
	return ipstack[ipstackptr--];
}
int peekip(){
	return ipstack[ipstackptr];
}

int load(int n){
	return memory[actualmod(n, word_max)];
}
void store(int m, int n){
	memory[actualmod(m, word_max)] = n;
}

int getnum(){
	int n = 0;
	c = program[pc++];
	int sign = 1;
	if(c=='-'){
		sign = -1;
		c = program[pc++];
	}
	while(c>='0' && c<='9'){
		n = n*10 + (c-'0');
		c = program[pc++];
	}
	pc--;
	return sign*n;
}

void printstack(){
	for(int i=0; i<=stackptr; i++){
		printf("%i ", stack[i]);
	}
}

void printmem(){
	for(int i=0; i<10; i++){
		printf("%i ", memory[i]);
	}
}

void step(){
	if(search!=0){
		switch(c){
			       case '(': search++;
			break; case ')': search--;
			break; case '{': search++;
			break; case '}': search--;
		}
	}else{
		if(outputdebug){
			printf("%04i ", pc);
			printf("%c ", c);
		}
		
		switch(c){
			       case '+': push(pop2() + pop());
			break; case '-': push(pop2() - pop());
			break; case '*': push(pop2() * pop());
			break; case '/': push(pop2() / pop());
			break; case '%': if(get(1)!=0){ int a = pop2(); int b = pop(); push(actualmod(a, b)); }else{ pop();pop();push(0); }
			break; case '~': push(-pop());
			break; case '^': push(pop()+1);
			break; case 'v': push(pop()-1);
			break; case '&': push(pop2() & pop());
			break; case '|': push(pop2() | pop());
			break; case '!': push(~pop());
			break; case '>': push(pop2() > pop());
			break; case '<': push(pop2() < pop());
			break; case '=': push(pop2() == pop());
			break; case 'n': push(pop() == 0);
			break; case 'l': push(pop2() << pop());
			break; case 'r': push(pop2() >> pop());
			break; case 'g': push(get(getnum()));
			break; case 's': set(getnum()-1, pop());
			break; case 'x': swap(getnum());
			break; case 'p': push(getnum());
			break; case 'w': swap(2);
			break; case 'q': pop();
			break; case 'd': push(peek());
			break; case '{': push(pc); search = 1;
			break; case '}': pc = popip();
			break; case 'c': puship(pc); pc = pop();
			break; case '?': push(inputn());
			break; case '\'': push(inputc());
			break; case '.': outputn(pop());
			break; case ',': outputc(pop());
			break; case '[': puship(pc);
			break; case ']': if(pop()!=0){ pc = peekip(); }else{ popip(); }
			break; case '(': if(pop()==0){ search = 1; }
			break; case ')': 
			break; case ';': push(load(pop()));
			break; case ':': {int m = pop2(); int n = pop(); store(m, n);}
			break; case '#': outputn(stackptr+1);
			break; case 'y': push(1);
			break; case 'z': push(0);
			break; case '@': push(load(getnum()));
			break; case '_': outputc('_');
			break; case ' ': 
			break; case '\t': 
			break; case '\r': 
			break; case '\n': 
			break; default: printf("invalid opcode \'%c\'", c);
		}
		
		if(outputdebug){
			printstack();
			printf("\n");
			//printmem();
			//printf("\n\n");
		}
	}
	c = program[pc++];
}

void begin(){
	stackptr = -1;
	ipstackptr = -1;
	
	pc = 0;
	search = 0;
	
	c = program[pc++];
	
	while(c!=0){
		step();
	}
}

const int PROG_LEN = 1024*1024;
const char* prog[PROG_LEN];

int main(int argc, const char* argv[]){
	if(argc<=1){
		printf("no program\n");
	}else{
		//printf("program \"%s\"\n", argv[1]);
		FILE* fin = fopen(argv[1], "r");
		if(fin==0){
			printf("file \"%s\" does not exist\n", argv[1]);
		}else{
			fread(prog, 1, PROG_LEN, fin);
			fclose(fin);
			
			program = (char*)prog;
			
			begin();
		}
	}
	
	return 0;
}
