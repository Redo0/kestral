
(   jump past | if 0
|   jump past )
)

[
]   jump past ] if 1

{   push ip and jump past }
}


(   if(search!=0) search++
|   if(search==1) search=0
)   if(search!=0) search--

[   puship ip
]   jump back if not 0

{   search++
}   search--

0-f const = const<<4 + immed (const becomes 1 on any access)
push const immed
exchange with const offset
get const offset
set const offset

pop
pop second from top

load
store
load from const addr
store at const addr

not zero
bitwise invert
bitwise and
bitwise or
bitwise nand
bitwise nor
bitwise xor
bitwise xnor
get bit
set bit
shl
shr

numeric negate
absolute value

add
subtract
multiply
divide
modulo

equal
not equal
less than
not less than
greater than
not greater than
