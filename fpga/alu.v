
module adder_16bit(
		a,
		b,
		sum,
		cin,
		cout
	);
	input[15:0] a;
	input[15:0] b;
	output[15:0] sum;
	input cin;
	output cout;
	
	assign sum = cin+a+b;
	assign cout = (cin+a+b)>=65536;
endmodule

module mux_4(
		a,
		b,
		line,
		out
	);
	input a;
	input b;
	input[3:0] line;
	output out;
	
	assign out = line[a+2*b];
endmodule

module mux_4_16bit(
		a,
		b,
		line,
		out
	);
	
	input[15:0] a;
	input[15:0] b;
	input[3:0] line;
	output[15:0] out;
	
	mux_4 mux0[15:0](a, b, line, out);
endmodule

module roller_right_16bit(
		in,
		amt,
		out
	);
	input[15:0] in;
	input[3:0] amt;
	output[15:0] out;
	
	assign out = (in>>amt) | (in<<(16-amt));
endmodule

module roller_left_16bit(
		in,
		amt,
		out
	);
	input[15:0] in;
	input[3:0] amt;
	output[15:0] out;
	
	assign out = (in<<amt) | (in>>(16-amt));
endmodule
