
//mem_getbyte = (mem_dout >> (24-(addr%4)*8)) & 'hff;


module memory_main(
		addr,
		din,
		dout,
		clk_write
	);
	input [31:0] addr;
	input [31:0] din;
	output [31:0] dout;
	input clk_write;
	
	reg [31:0] memory [(1<<8)-1:0];
	
	always @(posedge clk_write) begin
		memory[addr] = din;
	end
	
	initial begin
		memory['hf0] <= "p64.";
		memory['hf1] <= "#___";
	end
	
	assign dout = memory[addr];
endmodule

module byte_getter(
		word,
		offset,
		out
	);
	input [31:0] word;
	input [3:0] offset;
	output [7:0] out;
	
	assign out = (word >> (24 - offset*8)) & 'hff;
endmodule

module kestral_engine(
		clk,
		data_out,
		clk_out
	);
	input clk;
	output reg [31:0] data_out;
	output reg clk_out;
	
	
	//execution registers
	reg [31:0] pc;
	reg [31:0] immed;
	reg [31:0] search;
	reg [7:0] pgm_next;
	reg immed_search;
	
	
	//memory interface
	reg [31:0] mem_addr;
	reg [31:0] mem_din;
	wire [31:0] mem_dout;
	reg mem_clk_write;
	memory_main mem(mem_addr, mem_din, mem_dout, mem_clk_write);
	
	reg [3:0] byte_offset;
	wire [7:0] mem_byte;
	byte_getter getter(mem_dout, byte_offset, mem_byte);
	
	task mem_getbyte;
		input [31:0] addr;
		mem_addr = addr/4;
		byte_offset = addr%4;
	endtask
	
	task mem_write;
		input [31:0] addr;
		input [31:0] data;
		mem_addr = addr;
		mem_din = data;
		mem_clk_write = 1;
	endtask
	
	
	//input/output
	task output_data;
		input [31:0] data;
		data_out = data;
		clk_out = 1;
	endtask
	
	
	//stack
	reg [31:0] stack_ptr;
	task stack_pop;
		stack_ptr <= stack_ptr-1;
	endtask
	
	task stack_push;
		input [31:0] val;
		stack_ptr <= stack_ptr+1;
	endtask
	
	
	//setup
	initial begin
		pc <= 'hf0 * 4;
		immed <= 0;
		search <= 0;
		pgm_next <= 0;
		immed_search <= 0;
		clk_sys <= 0;
		clk_sys_int <= 0;
		stack_ptr <= 0;
	end
	
	reg [2:0] clk_sys_int;
	reg [7:0] clk_sys;
	
	//main clock
	always @(posedge clk) begin
		
	end
	
	always @(negedge clk) begin
		mem_clk_write = 0;
		clk_out = 0;
	end
endmodule

module kestral_testbench(
	);
	reg clk;
	wire clk_out;
	wire [31:0] data_out;
	
	kestral_engine eng(clk, data_out, clk_out);
	
	//always begin
	//	clk = 0;
	//	#5;
	//	
	//	clk = 1;
	//	#5;
	//	
	//	if(clk_out) begin
	//		$display("output %x", data_out);
	//	end
	//end
	
endmodule
