--love 1

local lg = love.graphics
local lf = love.font
local le = love.event
local li = love.image

local exec = require "evolve2back"
local step
local font

local drawscreen = false
local superspeed = false

local screenscale = 3
local screenposx = 512+128
local screenposy = 0
local stackposx = 512
local stackposy = 12

local word_length = 16
local word_max = 2^word_length
local word_half = word_max/2

local function makesigned(n)
	return n>=word_half and n-word_max or n
end

local prog, stack, istack, memory, ip, searching
local function step1()
	local running = true
	local didnext = false
	while (not didnext) and running do
		prog, stack, istack, memory, ip, running, didnext = step()
	end
	return running
end

function love.load()
	local filename = arg[2] or "programs\\test.kst"
	
	local file = io.open(filename, "r")
	if not file then
		print("file \""..filename.."\" does not exist")
	else
		local prograw = file:read("*a")
		file:close()
		step = exec(prograw)
		font = lg.newFont("consola.ttf", 12)
		step1()
	end
end

local run = false
local cycles = 0
function love.update(dt)
	if run then
		local count = superspeed and 100 or 1
		for i=1, count do
			if step1() then
				cycles = cycles+1
			end
		end
	end
end

local function instructionfrom(loc)
	local preip = prog:sub(1, loc-1)
	local atip = prog:sub(loc, #prog)
	if atip:match("^[ \t\n]") then
		atip = atip:match("^[ \t\n]+.%-?[0-9]*")
	else
		atip = atip:match("^.%-?[0-9]*")
	end
	atip = atip or ""
	local postip = prog:sub(loc+#atip, #prog)
	
	return preip, atip, postip
end

function love.draw()
	
	lg.setFont(font)
	
	lg.print(cycles, 32, 12*1)
	
	local preip, atip, postip = instructionfrom(ip)
	
	lg.print({
		{255, 255, 255}, preip,
		{255, 0, 0}, atip,
		{255, 255, 255}, postip,
	}, 32, 12*3)
	--lg.print((" "):rep(ip-2).."^", 32, 12*3)
	
	for stidx = 1, #stack do
		local st = makesigned(stack[stidx])
		local yp = stidx
		if yp<=31 then
			lg.print(st, stackposx, stackposy - 12 + 384-12*yp)
		end
	end
	
	local cols = 16
	for memidx = 0, 511 do
		local mem = memory[memidx] or 0
		lg.print(mem, 512 - 32 + 32*(memidx%cols) + 32, 384 + 10 + (11*math.floor(memidx/cols)))
	end
	
	
	local screenimgd = li.newImageData(128, 128)
	local screenoffset = 0xF800
	local pixofs = 0
	for memidx = 0, 128*128/16-1 do
		local word = memory[memidx+screenoffset] or 0
		for b = 1, 16 do
			local pixel = bit.band(word, bit.lshift(1, b-1))~=0
			local val = pixel and 255 or 0
			screenimgd:setPixel(pixofs%128, math.floor(pixofs/128), val, val, val, 255)
			pixofs = pixofs+1
		end
	end
	local screenimg = lg.newImage(screenimgd)
	lg.setDefaultFilter("nearest", "nearest")
	lg.draw(screenimg, screenposx, screenposy, 0, screenscale, screenscale)
	lg.rectangle("line", screenposx, screenposy, 128*screenscale, 128*screenscale)
end

function love.keypressed(k)
	if k=="escape" then le.quit()
	elseif k=="a" then step1()
	elseif k=="r" then run = not run
	elseif k=="s" then drawscreen = not drawscreen
	elseif k=="t" then superspeed = not superspeed
	end
end
