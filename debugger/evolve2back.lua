--love 1

local word_length = 16
local word_max = 2^word_length
local word_half = word_max/2

local function correct(n)
	return n%word_max
end
local function makesigned(n)
	return n>=word_half and n-word_max or n
end

local function input()
	return correct(math.floor(tonumber(io.read()) or 0))
end
local function inputc()
	return correct(string.byte(io.read(1)) or 0)
end
local function output(n)
	io.write(makesigned(n).." ")
end
local function outputc(n)
	io.write(string.char(n))
end

local stack = {}
local function push(n)
	stack[#stack+1] = n
end
local function pop()
	local r = stack[#stack]
	stack[#stack] = nil
	return r
end
local function peek()
	return stack[#stack]
end
local function change(n)
	stack[#stack] = n
end
local function get(n)
	stack[#stack+1] = stack[#stack-n+1]
end
local function set(n)
	stack[#stack-n+1] = stack[#stack]
	stack[#stack] = nil
end
local function swap(n)
	stack[#stack], stack[#stack-n+1] = stack[#stack-n+1], stack[#stack]
end
local function op1(f)
	stack[#stack] = correct(f(stack[#stack]))
end
local function op2(f)
	stack[#stack-1] = correct(f(stack[#stack-1], stack[#stack]))
	stack[#stack] = nil
end
local function op2n(f)
	f(stack[#stack-1], stack[#stack])
	stack[#stack] = nil
	stack[#stack] = nil
end

local function m_add(a, b) return a+b end
local function m_sub(a, b) return a-b end
local function m_mul(a, b) return a*b end
local function m_div(a, b) return math.floor(a/b) end
local function m_mod(a, b) return a%b end
local function m_neg(a)    return -a  end
local function m_inc(a)    return a+1 end
local function m_dec(a)    return a-1 end
local function m_and(a, b) return bit.band(a, b) end
local function m_or (a, b) return bit.bor(a, b) end
local function m_not(a)    return bit.bnot(a) end
local function m_shl(a, b) return bit.lshift(a, b) end
local function m_shr(a, b) return bit.rshift(a, b) end
local function m_gt (a, b) return a> b and 1 or 0 end
local function m_lt (a, b) return a< b and 1 or 0 end
local function m_ge (a, b) return a>=b and 1 or 0 end
local function m_le (a, b) return a<=b and 1 or 0 end
local function m_eq (a, b) return a==b and 1 or 0 end
local function m_ne (a, b) return a~=b and 1 or 0 end
local function m_nz (a)    return a==0 and 1 or 0 end

local memory = {}
local function m_load(a) return memory[a%word_max] or 0 end
local function m_store(a, b) memory[a%word_max] = b end

local istack = {}

local function pushloc(n)
	istack[#istack+1] = n
end
local function poploc()
	local r = istack[#istack]
	istack[#istack] = nil
	return r
end
local function peekloc()
	return istack[#istack]
end

local function convertnum(n)
	return correct(tonumber(n))
end

local ip
local search
local noop

local opcodes = {
	["+" ] = {function() op2(m_add) ip = ip+1 end},
	["-" ] = {function() op2(m_sub) ip = ip+1 end},
	["*" ] = {function() op2(m_mul) ip = ip+1 end},
	["/" ] = {function() op2(m_div) ip = ip+1 end},
	["%" ] = {function() op2(m_mod) ip = ip+1 end},
	["~" ] = {function() op1(m_neg) ip = ip+1 end},
	["^" ] = {function() op1(m_inc) ip = ip+1 end},
	["v" ] = {function() op1(m_dec) ip = ip+1 end},
	["&" ] = {function() op2(m_and) ip = ip+1 end},
	["|" ] = {function() op2(m_or ) ip = ip+1 end},
	["!" ] = {function() op1(m_not) ip = ip+1 end},
	[">" ] = {function() op2(m_gt ) ip = ip+1 end},
	["<" ] = {function() op2(m_lt ) ip = ip+1 end},
	["=" ] = {function() op2(m_eq ) ip = ip+1 end},
	["n" ] = {function() op1(m_nz ) ip = ip+1 end},
	--["l" ] = {function(n) push(convertnum(n)) op2(m_shl) ip = ip+1+#n end, "#"},
	--["r" ] = {function(n) push(convertnum(n)) op2(m_shr) ip = ip+1+#n end, "#"},
	["l" ] = {function() op2(m_shl) ip = ip+1 end},
	["r" ] = {function() op2(m_shr) ip = ip+1 end},
	["g" ] = {function(n) get (convertnum(n)) ip = ip+1+#n end, "#"},
	["s" ] = {function(n) set (convertnum(n)) ip = ip+1+#n end, "#"},
	["x" ] = {function(n) swap(convertnum(n)) ip = ip+1+#n end, "#"},
	["p" ] = {function(n) push(convertnum(n)) ip = ip+1+#n end, "#"},
	["w" ] = {function(n) swap(2) ip = ip+1 end},
	["q" ] = {function() pop() ip = ip+1 end},
	["d" ] = {function() get(1) ip = ip+1 end},
	["{" ] = {function() push(ip+1) search = 1 ip = ip+1 end},
	["}" ] = {function() ip = poploc()+1 end},
	["c" ] = {function() pushloc(ip) ip = pop() end},
	["?" ] = {function() push(input()) ip = ip+1 end},
	["'" ] = {function() push(inputc()) ip = ip+1 end},
	["." ] = {function() output(pop()) ip = ip+1 end},
	["," ] = {function() outputc(pop()) ip = ip+1 end},
	["[" ] = {function() pushloc(ip) ip = ip+1 end},
	["]" ] = {function() if pop()~=0 then ip = peekloc()+1 else poploc() ip = ip+1 end end},
	["(" ] = {function() if pop()==0 then search = 1 end ip = ip+1 end},
	[")" ] = {function() ip = ip+1 end},
	[";" ] = {function() op1(m_load) ip = ip+1 end},
	[":" ] = {function() op2n(m_store) ip = ip+1 end},
	["#" ] = {function() output(#stack) ip = ip+1 end},
	["y" ] = {function() push(1) ip = ip+1 end},
	["z" ] = {function() push(0) ip = ip+1 end},
	["@" ] = {function(n) push(convertnum(n)) op1(m_load) ip = ip+1+#n end, "#"},
	["_" ] = {function() outputc(("_"):byte()) ip = ip+1 end},
	[" " ] = {function() noop = true ip = ip+1 end},
	["\t"] = {function() noop = true ip = ip+1 end},
	["\r"] = {function() noop = true ip = ip+1 end},
	["\n"] = {function() noop = true ip = ip+1 end},
}

local function run(s)
	--s = s:gsub("[\t ]", " "):gsub(" +", " ")
	local function getchar(i)
		local c = s:sub(i, i)
		if #c==1 then return c else return nil end
	end
	
	ip = 1
	search = 0
	
	local c = getchar(ip)
	local function step()
		local didnext = false
		local running = false
		noop = false
		if c then
			if search~=0 then
				if c=="(" or c=="{" then search = search+1
				elseif c==")" or c=="}" then search = search-1
				end
				ip = ip+1
				c = getchar(ip)
				if search==0 then didnext = true end
			else
				local op = opcodes[c]
				if op then
					if op[2]=="#" then
						local n = ""
						local i = ip+1
						c = getchar(i)
						if c=="-" then
							n = "-"
							i = i+1
							c = getchar(i)
						end
						while true do
							if c and c:find("[0-9]") then
								n = n..c
							else
								break
							end
							i = i+1
							c = getchar(i)
						end
						op[1](n)
					else
						op[1]()
						c = getchar(ip)
					end
					didnext = (not noop) and search==0
				else
					print("invalid opcode "..c)
					ip = ip+1
					c = getchar(ip)
				end
			end
			
			--print(s)
			--print(string.rep(" ", ip-2).."^")
			--print(table.concat(stack, " "))
			--io.read()
			
			running = true
		end
		return s, stack, istack, memory, ip, running, didnext
	end
	
	return step
end

--factorial
--run("[?d{p1[g2*g2vds4].q}p1]")

--find primes up to n
--run("?[p1wdv[g2g2%p0={p0s4}vdv]qg2{d.}wqvdp2-]")

--find primes from n
--run("?[p1wdv[g2g2%p0={p0s4}vdv]qg2{d.}wq^d]")

--hello world
--run("p72,p101d,p7+ddd,,p3+d,p44d,p-12+,p87,,p114,,p100,p33,")

--garbage test
--run("?{this is some useless text}")

--print char values
--run("['d.p1]")

--echo text
--run("p0['dg3w:g2^s3p10=]#")

return run
