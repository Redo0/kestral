
--require "stablesort"

local shiftmax = 16

local function apply(map, midx, op)
	local nidx = midx+op
	if map[nidx] and map[nidx]>map[midx]+1 then
		map[nidx] = map[midx]+1
	end
end

local function expand(map, ops)
	local did = false
	for midx, m in ipairs(map) do
		for opidx, op in ipairs(ops) do
			did = did or apply(map, midx, op)
		end
	end
	return did
end

local function generate(length, shifts)
	local map = {}
	local ops = {}
	for i = 1, length do map[i] = 100 end
	for sidx, s in ipairs(shifts) do
		map[s] = map[s] and 1
		table.insert(ops, s)
		table.insert(ops, -s)
	end
	while expand(map, ops) do end
	return map
end

local function value(shifts)
	local map = generate(shiftmax*2, shifts)
	
	local sum = 0
	local max = 1
	for midx = 1, shiftmax-1 do
		local m = map[midx]
		sum = sum + m
		if m>max then max = m end
	end
	
	return sum, max, map[shiftmax-1]
end

local results = {}

for shift1 = 1, shiftmax-1 do
	for shift2 = shift1+1, shiftmax-1 do
		for shift3 = shift2+1, shiftmax-1 do
			local sum, max, m15 = value({shift1, shift2, shift3})
			if sum<100 then
				table.insert(results, {shift1, shift2, shift3, sum, max, m15})
			end
		end
	end
end

local function sortern(n)
	return function(a, b) return a[n]<b[n] end
end

table.sort(results, sortern(4))

for ridx, r in ipairs(results) do
	print(string.format("%2i %2i %2i: s=%2i, m=%2i, 15=%2i", r[1], r[2], r[3], r[4], r[5], r[6]))
end
